customer-group-minimum-order
============================

This is an Opencart extensions where you can set the minimum order per customer group per product. 

Requirement:

Opencart 1.5.6.1
VQMod 2.4.1

SQL:
DROP TABLE IF EXISTS `oc_product_minorder`;
CREATE TABLE `oc_product_minorder` (
 `product_minorder_id` int(11) NOT NULL AUTO_INCREMENT,
 `product_id` int(11) NOT NULL,
 `order` int(11) NOT NULL,
 `customer_group_id` int(11) NOT NULL,
 PRIMARY KEY (`product_minorder_id`),
 UNIQUE KEY `product_minorder_id` (`product_minorder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8