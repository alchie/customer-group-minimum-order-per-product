<?php
// Heading
$_['heading_title'] = 'Customer Group Mininum Order';

// Text
$_['text_module'] = 'Module';
$_['text_success'] = 'Success: You have modified module Customer Group Minimum Order!';
$_['text_name'] = 'Name';
$_['text_customer_group_min_order_help'] = '';

// Entry
$_['entry_customer_group_name'] = 'Customer Group:';
$_['entry_display_multiple_prices'] = 'Display other group prices on product detail page:';
$_['entry_customer_group_label'] = 'Customer Groups\' Minimum Order:';
$_['entry_customer_group_min_order'] = 'Minimum Order:';

// Error 
$_['error_permission'] = 'Warning: You do not have permission to modify this module!';

// Warning
?>
